;;; only run 1 instance of emacs
(server-start)

;; set up the color theme to use
;;(add-to-list 'load-path "/usr/share/emacs/site-lisp/emacs-goodies-el/color-theme.el")
(require 'color-theme)
(color-theme-blackboard)

;; auto complete
(defun indent-or-expand (arg)
  "Either indent according to mode, or expand the word preceding
point."
  (interactive "*P")
  (if (and
       (or (bobp) (= ?w (char-syntax (char-before))))
       (or (eobp) (not (= ?w (char-syntax (char-after))))))
      (dabbrev-expand arg)
    (indent-according-to-mode)))
 
(defun my-tab-fix ()
  (local-set-key [tab] 'indent-or-expand))

;; set up groovy syntax highlighting
;;; turn on syntax hilighting
(global-font-lock-mode 1)

;; set font size to 10pt
(set-face-attribute 'default nil :height 90)

;;; use groovy-mode when file ends in .groovy or has #!/bin/groovy at start
(autoload 'groovy-mode "groovy-mode" "Groovy editing mode." t)
(add-to-list 'auto-mode-alist '("\.groovy$" . groovy-mode))
(add-to-list 'interpreter-mode-alist '("groovy" . groovy-mode))

;;; set up the proxy
(setq url-using-proxy t)
(setq url-proxy-services
      '(("http"  . "[PROXY_GOES_HERE")))

;;; from http://www.khngai.com/emacs/cygwin.php
;;; set up the cygwin mounts so that it recognizes cygpaths
(setenv "PATH" (concat "c:/cygwin/bin;" (getenv "PATH")))
(setq exec-path (cons "c:/cygwin/bin/" exec-path))
(require 'cygwin-mount)
(cygwin-mount-activate)

;;; make the default shell a cygwin shell
;;; http://www.khngai.com/emacs/cygwin.php
(add-hook 'comint-output-filter-functions
    'shell-strip-ctrl-m nil t)
(add-hook 'comint-output-filter-functions
    'comint-watch-for-password-prompt nil t)
(setq explicit-shell-file-name "bash.exe")
;; For subprocesses invoked via the shell
;; (e.g., "shell -c command")(setq shell-file-name explicit-shell-file-name)

;;; show current directory in mode line
(defun add-mode-line-dirtrack ()
  (add-to-list 'mode-line-buffer-identification 
               '(:propertize (" " default-directory " ") face dired-directory)))
(add-hook 'shell-mode-hook 'add-mode-line-dirtrack)


