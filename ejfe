#!/bin/sh

# A script which will extract a file from a jar file into a temporary directory
# and allow me to edit the contents, before updating the jar file

# The name of the temp directory
TEMP_DIR=.tmp-jar-file-extract.`date +%s`

# check if we have an EJFE_EDITOR defined
if [[ "$EJFE_EDITOR" == "" ]]
then
    EJFE_EDITOR=gvim
fi

# check we can find the editor
EDITOR_PATH=`which $EJFE_EDITOR 2>&1`
if [[ ! -e "$EDITOR_PATH" ]]
then
    echo "$EJFE_EDITOR not found - using vi"
    EJFE_EDITOR=vi
fi

# Check we have 2 arguments
if [[ -z "$1" || -z "$2" ]]
then
    echo "Usage: ejfe [jarfile] [file-path]"
    exit -1
fi

# We need to know the jar file and the name of the file to extract
JARFILE=$1
TARGET_FILE=$2

# Test that the jar file exists
if [[ ! -e "$JARFILE" || ! -f "$JARFILE" ]]
then
    echo "Jar file '$JARFILE' does not exist or is a directory"
    exit -1
fi

# Check that it looks like a jar file
FILETYPE=`file -bi "$JARFILE"`

if [[ ! "$FILETYPE" == "application/x-zip" ]]
then
    echo "$JARFILE is unrecognized filetype. Expected a jar file..."
    exit -1
fi


# Try to extract the file
mkdir $TEMP_DIR
cd $TEMP_DIR

jar xf ../$JARFILE $TARGET_FILE

# Check that the file exists
if [[ -e "$TARGET_FILE" ]]
then
    # Store the last modified time
    LAST_MOD=`stat --format=%Y $TARGET_FILE`

    # Try to edit the file
    $EJFE_EDITOR $TARGET_FILE

    # check if the file has been modified
    NEW_LAST_MOD=`stat --format=%Y $TARGET_FILE`

    if [[ $LAST_MOD == $NEW_LAST_MOD ]]
    then
	echo "$TARGET_FILE not modified"
    else
    	# Update the file in the jar
	jar uf ../$JARFILE $TARGET_FILE
    fi
else
    echo "Error: $TARGET_FILE does not exist"
fi

cd ..
rm -rf $TEMP_DIR
