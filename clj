#!/bin/bash

# NB: ALTHOUGH THIS IS A BASH SCRIPT IT IS DESIGNED TO RUN ON CYGWIN
# TO RUN ON LINUX THE CLASSPATH SEPARATOR NEEDS TO BE SPECIFIED DIFFERENTLY
# SHOULD EXTEND THE SCRIPT TO TEST FOR THIS TO MAKE IT RUNNABLE ON BOTH

# Simple script that launches clojure allowing the classpath to be specified
# in a number of different ways
# --pom       Build the CLASSPATH using the pom.xml file in the current dir
# --pomclj    Same as --pom but also adds clojure, clojure-contrib and swank-clojure to CLASSPATH (for non clojure maven projects)
# --classpath Add the arg to the CLASSPATH. Can be combined with pom etc
#
# There are also ways to run specific commands:
# -e [s]      Execute the Clojure commands contained in s
# --swank     Start a Swank server (assumes you have passed the params to build the CLASSPATH correctly)
#
# So, to start a swank server in a vanilla Maven project, do:
# clj --pomclj --swank

function display_help {
    cat <<EndOfHelp
Simple script that launches clojure allowing the classpath to be specified
in a number of different ways
--pom       Build the CLASSPATH using the pom.xml file in the current dir
--pomclj    Same as --pom but also adds clojure, clojure-contrib and swank-clojure to CLASSPATH (for non clojure maven projects)
--classpath Add the arg to the CLASSPATH. Can be combined with pom etc

There are also ways to run specific commands:
-e [s]      Execute the Clojure commands contained in s
--swank     Start a Swank server (assumes you have passed the params to build the CLASSPATH correctly)

So, to start a swank server in a vanilla Maven project, do:
clj --pomclj --swank
EndOfHelp
}

# adds each argument to the classpath, making sure it's converted to windows style
function add_to_classpath {
    until [ -z "$1" ]
    do
        ITEM=`cygpath --windows $1`

        if [ $VERBOSE ]
        then
            echo Add to classpath: $ITEM
        fi
        
        CLASSPATH="$CLASSPATH;$ITEM"
        shift
    done
}

function init_classpath {
    # set up the classpath dynamically. Note this includes the jline jar
    for ITEM in `ls -l ~/.clojure-classpath/ | pcol 11`; do
        add_to_classpath "$ITEM"
    done
}

function add_clojure_to_classpath {
    # a way to be able to start a clojure session using a pom
    # by also adding clojure and swank etc to the classpath
    for ITEM in `ls -l ~/.clojure-classpath/*clojure*.jar | pcol 11`; do
        add_to_classpath "$ITEM"
    done
}

function init_classpath_from_pom {
    # create the classpath file
    mvn dependency:build-classpath -Dmdep.outputFile=classpath 2>&1 > /dev/null

    # store it and remove the file
    add_to_classpath `more classpath | sed -e 's/;/ /g'`
    rm classpath

    # also add everything under the clojure source directories
    # include ./src for leiningen based projects
    add_to_classpath "./src/main/clojure" "./src/test/clojure" "./src"

    # also add classes directory for maven projects
    add_to_classpath "./target/classes"
}
 
# Build the classpath and run clojure
if [ $# -eq 0 ] ; then
    init_classpath
    stty -icanon min 1 -echo
    java -Djline.terminal=jline.UnixTerminal -cp $CLASSPATH jline.ConsoleRunner clojure.main
else
    TMPFILE=""
    while [ $# -gt 0 ] ; do
        case "$1" in
            --verbose)
                VERBOSE=1
                ;;
            --swank)
                TMPFILE="/tmp/$(basename $0).$$.swank.tmp"
                /bin/echo "(require 'swank.swank)" > $TMPFILE
                /bin/echo "(swank.swank/start-server \"nul\" :encoding \"utf-8\" :port 4005)" >> $TMPFILE
                ARGS=$TMPFILE
                ;;
            --pom)
                init_classpath_from_pom
                ;;
            --pomclj)
                init_classpath_from_pom
                add_clojure_to_classpath
                ;;
            --classpath)
                CLASSPATH="$CLASSPATH;$2"
                shift
                ;;
            -e)
                TMPFILE="/tmp/$(basename $0).$$.tmp"
                /bin/echo $2 > $TMPFILE
                ARGS=$TMPFILE
                ;;
            -h|--help)
                display_help
                exit 1
                ;;
            *)
                ARGS="$ARGS $1"
                ;;
        esac
        shift
    done

    if [ "$CLASSPATH" == "" ] ; then
        init_classpath
    fi

    if [ "$ARGS" != "" ]; then 
        ARGS=`cygpath --windows $ARGS`
    fi
 
    java -cp "$CLASSPATH" clojure.main $ARGS
    if [ "$TMPFILE" != "" ] ; then
        rm $TMPFILE
    fi
fi
